package com.steveedmans.shopping.model

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by Steve Edmans on 04/05/2017.
  */
class OfferSpec extends FlatSpec with Matchers {
  "A Buy Some Get One Free" should "replace the two Apples in basket with this offer" in {
    val basket = List(Apple, Apple)
    val actual = new BuySomeGetOneFree(Apple, 2).replaceAll(basket)
    actual should equal (List(BSGOF(Apple, 2)))
  }

  it should "replace the two Oranges in basket with this offer" in {
    val basket = List(Orange, Orange)
    val actual = new BuySomeGetOneFree(Orange, 2).replaceAll(basket)
    actual should equal (List(BSGOF(Orange, 2)))
  }

  it should "handle products that are not in the apple offer" in {
    val basket = List(Apple, Apple, Orange)
    val actual = new BuySomeGetOneFree(Apple, 2).replaceAll(basket)
    actual should equal (List(Orange, BSGOF(Apple, 2)))
  }

  it should "handle products that are not in the orange offer" in {
    val basket = List(Apple, Orange, Orange)
    val actual = new BuySomeGetOneFree(Orange, 2).replaceAll(basket)
    actual should equal (List(BSGOF(Orange, 2), Apple))
  }

  it should "handle a single product in the apple offer" in {
    val basket = List(Apple, Orange)
    val actual = new BuySomeGetOneFree(Apple, 2).replaceAll(basket)
    actual should equal (List(Apple, Orange))
  }

  it should "handle a single product in the orange offer" in {
    val basket = List(Apple, Orange)
    val actual = new BuySomeGetOneFree(Orange, 2).replaceAll(basket)
    actual should equal (List(Orange, Apple))
  }

  it should "handle a three products in the apple offer" in {
    val basket = List(Apple, Orange, Apple, Apple)
    val actual = new BuySomeGetOneFree(Apple, 2).replaceAll(basket)
    actual should equal (List(Apple, BSGOF(Apple, 2), Orange))
  }

  it should "handle a three products in the orange offer" in {
    val basket = List(Orange, Orange, Apple, Orange)
    val actual = new BuySomeGetOneFree(Orange, 2).replaceAll(basket)
    actual should equal (List(Orange, Apple, BSGOF(Orange, 2)))
  }

  it should "handle four apples in a basket and a BOGOF apple offer" in {
    val basket = List(Apple, Apple, Apple, Apple)
    val actual = new BuySomeGetOneFree(Apple, 2).replaceAll(basket)
    actual should equal (List(BSGOF(Apple, 2), BSGOF(Apple, 2)))
  }

  it should "handle four oranges in a basket and a BOGOF orange offer" in {
    val basket = List(Orange, Orange, Orange, Orange)
    val actual = new BuySomeGetOneFree(Orange, 2).replaceAll(basket)
    actual should equal (List(BSGOF(Orange, 2), BSGOF(Orange, 2)))
  }

  it should "handle an empty basket for the apple offer" in {
    val basket = List()
    val actual = new BuySomeGetOneFree(Apple, 2).replaceAll(basket)
    actual should equal (List())
  }

  it should "handle an empty basket for the orange offer" in {
    val basket = List()
    val actual = new BuySomeGetOneFree(Orange, 2).replaceAll(basket)
    actual should equal (List())
  }

  it should "handle buy 3 for the price of 2 for apples" in {
    val basket = List(Apple, Apple, Apple, Orange)
    val actual = new BuySomeGetOneFree(Apple, 3).replaceAll(basket)
    actual should equal (List(Orange, BSGOF(Apple, 3)))
  }

  it should "handle buy 3 for the price of 2 for oranges" in {
    val basket = List(Orange, Orange, Apple, Orange)
    val actual = new BuySomeGetOneFree(Orange, 3).replaceAll(basket)
    actual should equal (List(BSGOF(Orange, 3), Apple))
  }

  it should "handle buy 3 for the price of 2 for apples with 4 apples in the basket" in {
    val basket = List(Apple, Apple, Apple, Apple)
    val actual = new BuySomeGetOneFree(Apple, 3).replaceAll(basket)
    actual should equal (List(Apple, BSGOF(Apple, 3)))
  }

  it should "handle buy 3 for the price of 2 for oranges with 4 oranges in the basket" in {
    val basket = List(Orange, Orange, Orange, Orange)
    val actual = new BuySomeGetOneFree(Orange, 3).replaceAll(basket)
    actual should equal (List(Orange, BSGOF(Orange, 3)))
  }
}
