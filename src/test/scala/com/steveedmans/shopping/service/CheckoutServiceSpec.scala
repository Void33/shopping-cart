package com.steveedmans.shopping.service

import com.steveedmans.shopping.model._
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by Steve Edmans on 04/05/2017.
  */
class CheckoutServiceSpec extends FlatSpec with Matchers with MockFactory {
  "A checkout service" should "calaculate the total price of an orange" in {
    val basket = List(Orange)
    val service = new CheckoutService(List())
    val actual = service.totalPrice(basket)
    actual shouldBe BigDecimal("0.25")
  }

  it should "calculate the total price of a list of shopping items" in {
    val basket = List(Apple, Apple, Orange, Apple)
    val service = new CheckoutService(List())
    val actual = service.totalPrice(basket)
    actual shouldBe BigDecimal("2.05")
  }

  it should "determine that the total price of an empty basic is zero" in {
    val service = new CheckoutService(List())
    val actual = service.totalPrice(List())
    actual shouldBe BigDecimal("0")
  }

  it should "calculate the total price of a shopping basic of textual items" in {
    val basket = List("Apple", "Apple", "Orange", "Apple")
    val service = new CheckoutService(List())
    val actual = service.totalStringPrice(basket)
    actual shouldBe BigDecimal("2.05")
  }

  it should "allow you to specify offers" in {
    val mockOffer = mock[Offer]
    (mockOffer.replaceAll _).expects(List(Apple, Apple)).returning(List(BSGOF(Apple, 2)))
    val service = new CheckoutService(List(mockOffer))
    val basket = List(Apple, Apple)
    val actual = service.totalPrice(basket)
    actual shouldBe BigDecimal("0.60")
  }

  it should "allow you to handle a BOGOF apple offer" in {
    val appleOffer = new BuySomeGetOneFree(Apple, 2)
    val service = new CheckoutService(List(appleOffer))
    val basket = List(Apple, Apple)
    val actual = service.totalPrice(basket)
    actual shouldBe BigDecimal("0.60")
  }

  it should "allow you to handle a BOGOF orange offer" in {
    val orangeOffer = new BuySomeGetOneFree(Orange, 2)
    val service = new CheckoutService(List(orangeOffer))
    val basket = List(Orange, Orange)
    val actual = service.totalPrice(basket)
    actual shouldBe BigDecimal("0.25")
  }

  it should "allow you to handle a BOGOF apple offer and a BOGOF orange offer" in {
    val appleOffer = new BuySomeGetOneFree(Apple, 2)
    val orangeOffer = new BuySomeGetOneFree(Orange, 2)
    val service = new CheckoutService(List(orangeOffer, appleOffer))
    val basket = List(Orange, Apple, Orange, Apple)
    val actual = service.totalPrice(basket)
    actual shouldBe BigDecimal("0.85")
  }

  it should "allow you to handle a buy 3 apples for the price of 2 offer" in {
    val appleOffer = new BuySomeGetOneFree(Apple, 3)
    val service = new CheckoutService(List(appleOffer))
    val basket = List(Apple, Apple, Orange, Apple, Apple)
    val actual = service.totalPrice(basket)
    actual shouldBe BigDecimal("2.05")
  }

  it should "allow you to handle a buy 3 oranges for the price of 2 offer" in {
    val orangeOffer = new BuySomeGetOneFree(Orange, 3)
    val service = new CheckoutService(List(orangeOffer))
    val basket = List(Orange, Orange, Apple, Orange, Orange)
    val actual = service.totalPrice(basket)
    actual shouldBe BigDecimal("1.35")
  }
}
