package com.steveedmans.shopping.model

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by Steve Edmans on 04/05/2017.
  */
class ShoppingItemSpec extends FlatSpec with Matchers {
  "An apple" should "be a shopping item" in {
    val actual = Apple

    actual shouldBe a [ShoppingItem]
  }

  it should "cost 60p" in {
    val actual: ShoppingItem = Apple

    actual.cost shouldBe BigDecimal("0.60")
  }

  "An orange" should "be a shopping item" in {
    val actual = Orange

    actual shouldBe a [ShoppingItem]
  }

  it should "cost 25p" in {
    val actual: ShoppingItem = Orange

    actual.cost shouldBe BigDecimal("0.25")
  }

  "A banana" should "be a shopping item" in {
    val actual = Banana

    actual shouldBe a [ShoppingItem]
  }

  it should "cost 20p" in {
    val actual = Banana

    actual.cost shouldBe BigDecimal("0.20")
  }

  "A shopping item" should "convert the string 'Apple' to an Apple object" in {
    val  actual = ShoppingItem("Apple")

    actual should be theSameInstanceAs Apple
  }

  it should "convert the string 'Orange' to an Orange object" in {
    val  actual = ShoppingItem("Orange")

    actual should be theSameInstanceAs Orange
  }

  it should "convert the string 'OrAnGe' to an Orange object" in {
    val  actual = ShoppingItem("OrAnGe")

    actual should be theSameInstanceAs Orange
  }

  "A Buy Some Get One Free" should "cost the same as a single item of the same product when buying 2" in {
    val actual = BSGOF(Apple, 2).cost

    actual should equal (Apple.cost)
  }

  it should "cost the same as two items of the same product when buying 3" in {
    val actual = BSGOF(Apple, 3).cost
    actual should equal (Apple.cost.*(2))
  }

  "A BOGOF2" should "with Apple and Banana cost the same as the Apple" in {
    val actual = BOGOF2Item(Apple, Banana).cost
    actual should equal (Apple.cost)
  }

  it should "with Apple and Apple cost the same as the Apple" in {
    val actual = BOGOF2Item(Apple, Apple).cost
    actual should equal (Apple.cost)
  }
}
