package com.steveedmans.shopping.model

import com.steveedmans.shopping.service.CheckoutService.ShoppingBasket

/**
  * Created by Steve Edmans on 04/05/2017.
  */

/**
  * An offer that is available at the checkout.
  */
trait Offer {
  /**
    * Examine the shopping basket and convert any items that match the offer with an offer specific item.
    * @param basket The current shopping basket.
    * @return The shopping basket with the products replaced by offers.
    */
  def replaceAll(basket: ShoppingBasket) : ShoppingBasket
}

/**
  * An offer that lets the user buy multiple of the same item
  * and get the price of the last item off.
  * @param product The item the offer is for
  * @param threshold The number of items need to be bought to get the offer.
  */
class BuySomeGetOneFree(product: ShoppingItem, threshold : Int) extends Offer {
  /**
    * Replace multiple copies of the items with a buy some get one off item
    * @param basket The current shopping basket.
    * @return The shopping basket with the products replaced by offers.
    */
  override def replaceAll(basket: ShoppingBasket): ShoppingBasket = {
    replaceRecursive(basket, List(), counter = 0)
  }

  /**
    * Tail recursive version of the replace all function
    * @param basketRemaining The basket that has not been processed
    * @param output The items to be returned
    * @param counter The number of items of the product currently found
    * @return The shopping basket with the products replaced by offers.
    */
  def replaceRecursive(basketRemaining: ShoppingBasket, output: ShoppingBasket, counter: Int): ShoppingBasket = {
    basketRemaining match {
      case List() if counter > 0 => product :: output
      case List() => output
      case item :: rest if (item == product) && counter >= (threshold - 1) =>
        val newOutput = BSGOF(product, threshold) :: output
        replaceRecursive(rest, newOutput, counter = 0)
      case item :: rest if item == product => replaceRecursive(rest, output, counter = counter + 1)
      case item :: rest =>
        val newOutput = item :: output
        replaceRecursive(rest, newOutput, counter)
    }
  }
}
