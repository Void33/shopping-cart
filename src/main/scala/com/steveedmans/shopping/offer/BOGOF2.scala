package com.steveedmans.shopping.offer

import com.steveedmans.shopping.model.{BOGOF2Item, Offer, ShoppingItem}
import com.steveedmans.shopping.service.CheckoutService.ShoppingBasket

/**
  * Created by steveedmans on 06/06/2017.
  */
class BOGOF2(product1 : ShoppingItem, product2 : ShoppingItem) extends Offer {
  override def replaceAll(basket: ShoppingBasket): ShoppingBasket = {
    val (products, remaining) = basket.span(item => (item == product1) || (item == product2))
    val (firstProducts, secondProducts) = products.span(_ == product1)

    // Create offers with both products
    val bothCombined = firstProducts.zip(secondProducts).map(_ => BOGOF2Item(product1, product2))

    // Find the remaining items
    val remainingFirstProducts = firstProducts.drop(bothCombined.size)
    val remainingSecondProducts = secondProducts.drop(bothCombined.size)
    val (singleItemOffers, remainingItems) = if (remainingFirstProducts.size != 0) {
      findSingleItemOffers(remainingFirstProducts, product1)
    } else {
      findSingleItemOffers(remainingSecondProducts, product2)
    }
    remaining ++ bothCombined ++ singleItemOffers ++ remainingItems
  }

  def findSingleItemOffers(productsRemaining : List[ShoppingItem], product : ShoppingItem) = {
    val offers = Range(0, productsRemaining.size - 1, 2).map(_ => BOGOF2Item(product, product))
    val remaining = productsRemaining.drop(offers.size * 2)
    (offers, remaining)
  }
}
