package com.steveedmans.shopping.model

/**
  * Created by Steve Edmans on 04/05/2017.
  */

/**
  * A generic item that can be bought through the service.
  */
sealed trait ShoppingItem {
  val cost: BigDecimal
}

/**
  * The types of product that can be bought.
  *
  * Note: In a full system this would come from some for of database.
  */

case object Apple extends ShoppingItem {
  override val cost: BigDecimal = BigDecimal("0.60")
}

case object Orange extends ShoppingItem {
  override val cost: BigDecimal = BigDecimal("0.25")
}

case object Banana extends ShoppingItem {
  override val cost: BigDecimal = BigDecimal("0.20")
}


/**
  * Buy Some Get One Free
  * @param item The items bought
  * @param threshold The number of items
  */
case class BSGOF(item: ShoppingItem, threshold: Int) extends ShoppingItem {
  override val cost: BigDecimal = item.cost * (threshold - 1)
}

case class BOGOF2Item(product1 : ShoppingItem, product2 : ShoppingItem) extends ShoppingItem {
  override val cost: BigDecimal = if (product1.cost >= product2.cost) product1.cost else product2.cost
}

/**
  * Factory for a shopping item.
  */
object ShoppingItem {
  /**
    * Convert from a string to an instance of the relevant shopping item.
    *
    * Note : Using agile techniques this is the simplest solution to meet
    * the requirement, however it does not scale well and we would need to
    * change this to something else in a full system.
    *
    * TODO : We would need to determine what we want to do if the name
    * of the item is not known to us.
    *
    * @param name The name of the product (case insensitive)
    * @return The product bought.
    */
  def apply(name: String) : ShoppingItem = {
    name.toLowerCase match {
      case "apple" => Apple
      case "orange" => Orange
    }
  }
}