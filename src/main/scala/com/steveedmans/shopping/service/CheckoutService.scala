package com.steveedmans.shopping.service

import com.steveedmans.shopping.model.{Offer, ShoppingItem}

/**
  * Created by Steve Edmans on 04/05/2017.
  */

object CheckoutService {
  /**
    * A shopping basket is a list of shopping items
    */
  type ShoppingBasket = List[ShoppingItem]

  /**
    * A shopping basket where the products are represented as strings is a string shopping basket
    */
  type StringShoppingBasket = List[String]
}

/**
  * A checkout service.
  * @param offers A list of offers that are currently available.
  */
class CheckoutService(offers: List[Offer]) {
  import com.steveedmans.shopping.service.CheckoutService._

  /**
    * Determine the total price of a shopping basket
    * @param basket The shopping basket to price
    * @return The total price of the basket
    */
  def totalPrice(basket: ShoppingBasket) : BigDecimal = {
    // Process any offers that are available.
    val basketWithOffers = offers.foldLeft[ShoppingBasket](basket) {
      case (currentBasket, offer) => offer.replaceAll(currentBasket)
    }

    // Determine the price of the basket after the offers have been processed.
    basketWithOffers.foldLeft[BigDecimal](BigDecimal(0)) {
      case (accumulator, currentItem) => accumulator.+(currentItem.cost)
    }
  }

  /**
    * Determine the total price of a shopping basket
    *
    * TODO: We need to handle products that we do not recognize.
    *
    * @param basket The shopping basket, with items as text, to price
    * @return The total price of the basket
    */
  def totalStringPrice(basket: StringShoppingBasket) : BigDecimal = {
    totalPrice(basket.map(ShoppingItem(_)))
  }
}
