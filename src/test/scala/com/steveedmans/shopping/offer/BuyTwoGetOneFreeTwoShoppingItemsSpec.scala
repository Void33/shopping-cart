package com.steveedmans.shopping.offer

import com.steveedmans.shopping.model._
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by steveedmans on 06/06/2017.
  */
class BuyTwoGetOneFreeTwoShoppingItemsSpec extends FlatSpec with Matchers {
  "A BOGOF2" should "replace a Apple and a Banana in basket with this offer" in {
    val basket = List(Apple, Banana)
    val actual = new BOGOF2(Apple, Banana).replaceAll(basket)
    actual should equal (List(BOGOF2Item(Apple, Banana)))
  }

  it should "replace 6 Apples and 3 Bananas with four offers" in {
    val basket = List(Apple, Apple, Apple, Apple, Apple, Apple, Banana, Banana, Banana)
    val actual = new BOGOF2(Apple, Banana).replaceAll(basket)
    actual should equal (List(BOGOF2Item(Apple, Banana),BOGOF2Item(Apple, Banana),BOGOF2Item(Apple, Banana),BOGOF2Item(Apple, Apple), Apple))
  }

  it should "replace 3 Apples, 6 Bananas and 3 Oranges with four offers" in {
    val basket = List(Apple, Apple, Apple, Banana, Banana, Banana, Banana, Banana, Banana, Orange, Orange, Orange)
    val actual = new BOGOF2(Apple, Banana).replaceAll(basket)
    actual should equal (List(Orange, Orange, Orange, BOGOF2Item(Apple, Banana),BOGOF2Item(Apple, Banana),BOGOF2Item(Apple, Banana),BOGOF2Item(Banana, Banana), Banana))
  }
}
