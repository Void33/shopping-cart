name := "shopping"

version := "1.0"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.3" % "test",
  "org.scalamock" %% "scalamock-scalatest-support" % "3.5.0" % "test"
)